<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatealbumAPIRequest;
use App\Http\Requests\API\UpdatealbumAPIRequest;
use App\Models\album;
use App\Repositories\albumRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class albumController
 * @package App\Http\Controllers\API
 */

class albumAPIController extends AppBaseController
{
    /** @var  albumRepository */
    private $albumRepository;

    public function __construct(albumRepository $albumRepo)
    {
        $this->albumRepository = $albumRepo;
    }

    /**
     * Display a listing of the album.
     * GET|HEAD /albums
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $albums = $this->albumRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($albums->toArray(), 'Albums retrieved successfully');
    }

    /**
     * Store a newly created album in storage.
     * POST /albums
     *
     * @param CreatealbumAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatealbumAPIRequest $request)
    {
        $input = $request->all();

        $album = $this->albumRepository->create($input);

        return $this->sendResponse($album->toArray(), 'Album saved successfully');
    }

    /**
     * Display the specified album.
     * GET|HEAD /albums/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var album $album */
        $album = $this->albumRepository->find($id);

        if (empty($album)) {
            return $this->sendError('Album not found');
        }

        return $this->sendResponse($album->toArray(), 'Album retrieved successfully');
    }

    /**
     * Update the specified album in storage.
     * PUT/PATCH /albums/{id}
     *
     * @param int $id
     * @param UpdatealbumAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatealbumAPIRequest $request)
    {
        $input = $request->all();

        /** @var album $album */
        $album = $this->albumRepository->find($id);

        if (empty($album)) {
            return $this->sendError('Album not found');
        }

        $album = $this->albumRepository->update($input, $id);

        return $this->sendResponse($album->toArray(), 'album updated successfully');
    }

    /**
     * Remove the specified album from storage.
     * DELETE /albums/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var album $album */
        $album = $this->albumRepository->find($id);

        if (empty($album)) {
            return $this->sendError('Album not found');
        }

        $album->delete();

        return $this->sendResponse($id, 'Album deleted successfully');
    }
}
