<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateappmenuAPIRequest;
use App\Http\Requests\API\UpdateappmenuAPIRequest;
use App\Models\appmenu;
use App\Repositories\appmenuRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class appmenuController
 * @package App\Http\Controllers\API
 */

class appmenuAPIController extends AppBaseController
{
    /** @var  appmenuRepository */
    private $appmenuRepository;

    public function __construct(appmenuRepository $appmenuRepo)
    {
        $this->appmenuRepository = $appmenuRepo;
    }

    /**
     * Display a listing of the appmenu.
     * GET|HEAD /appmenus
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $appmenus = $this->appmenuRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($appmenus->toArray(), 'Appmenus retrieved successfully');
    }

    /**
     * Store a newly created appmenu in storage.
     * POST /appmenus
     *
     * @param CreateappmenuAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateappmenuAPIRequest $request)
    {
        $input = $request->all();

        $appmenu = $this->appmenuRepository->create($input);

        return $this->sendResponse($appmenu->toArray(), 'Appmenu saved successfully');
    }

    /**
     * Display the specified appmenu.
     * GET|HEAD /appmenus/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var appmenu $appmenu */
        $appmenu = $this->appmenuRepository->find($id);

        if (empty($appmenu)) {
            return $this->sendError('Appmenu not found');
        }

        return $this->sendResponse($appmenu->toArray(), 'Appmenu retrieved successfully');
    }

    /**
     * Update the specified appmenu in storage.
     * PUT/PATCH /appmenus/{id}
     *
     * @param int $id
     * @param UpdateappmenuAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateappmenuAPIRequest $request)
    {
        $input = $request->all();

        /** @var appmenu $appmenu */
        $appmenu = $this->appmenuRepository->find($id);

        if (empty($appmenu)) {
            return $this->sendError('Appmenu not found');
        }

        $appmenu = $this->appmenuRepository->update($input, $id);

        return $this->sendResponse($appmenu->toArray(), 'appmenu updated successfully');
    }

    /**
     * Remove the specified appmenu from storage.
     * DELETE /appmenus/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var appmenu $appmenu */
        $appmenu = $this->appmenuRepository->find($id);

        if (empty($appmenu)) {
            return $this->sendError('Appmenu not found');
        }

        $appmenu->delete();

        return $this->sendResponse($id, 'Appmenu deleted successfully');
    }
}
