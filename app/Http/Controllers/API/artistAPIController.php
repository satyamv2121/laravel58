<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateartistAPIRequest;
use App\Http\Requests\API\UpdateartistAPIRequest;
use App\Models\artist;
use App\Repositories\artistRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class artistController
 * @package App\Http\Controllers\API
 */

class artistAPIController extends AppBaseController
{
    /** @var  artistRepository */
    private $artistRepository;

    public function __construct(artistRepository $artistRepo)
    {
        $this->artistRepository = $artistRepo;
    }

    /**
     * Display a listing of the artist.
     * GET|HEAD /artists
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $artists = $this->artistRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($artists->toArray(), 'Artists retrieved successfully');
    }

    /**
     * Store a newly created artist in storage.
     * POST /artists
     *
     * @param CreateartistAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateartistAPIRequest $request)
    {
        $input = $request->all();

        $artist = $this->artistRepository->create($input);

        return $this->sendResponse($artist->toArray(), 'Artist saved successfully');
    }

    /**
     * Display the specified artist.
     * GET|HEAD /artists/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var artist $artist */
        $artist = $this->artistRepository->find($id);

        if (empty($artist)) {
            return $this->sendError('Artist not found');
        }

        return $this->sendResponse($artist->toArray(), 'Artist retrieved successfully');
    }

    /**
     * Update the specified artist in storage.
     * PUT/PATCH /artists/{id}
     *
     * @param int $id
     * @param UpdateartistAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateartistAPIRequest $request)
    {
        $input = $request->all();

        /** @var artist $artist */
        $artist = $this->artistRepository->find($id);

        if (empty($artist)) {
            return $this->sendError('Artist not found');
        }

        $artist = $this->artistRepository->update($input, $id);

        return $this->sendResponse($artist->toArray(), 'artist updated successfully');
    }

    /**
     * Remove the specified artist from storage.
     * DELETE /artists/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var artist $artist */
        $artist = $this->artistRepository->find($id);

        if (empty($artist)) {
            return $this->sendError('Artist not found');
        }

        $artist->delete();

        return $this->sendResponse($id, 'Artist deleted successfully');
    }
}
