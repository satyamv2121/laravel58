<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatefestivalAPIRequest;
use App\Http\Requests\API\UpdatefestivalAPIRequest;
use App\Models\festival;
use App\Repositories\festivalRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class festivalController
 * @package App\Http\Controllers\API
 */

class festivalAPIController extends AppBaseController
{
    /** @var  festivalRepository */
    private $festivalRepository;

    public function __construct(festivalRepository $festivalRepo)
    {
        $this->festivalRepository = $festivalRepo;
    }

    /**
     * Display a listing of the festival.
     * GET|HEAD /festivals
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $festivals = $this->festivalRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($festivals->toArray(), 'Festivals retrieved successfully');
    }

    /**
     * Store a newly created festival in storage.
     * POST /festivals
     *
     * @param CreatefestivalAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatefestivalAPIRequest $request)
    {
        $input = $request->all();

        $festival = $this->festivalRepository->create($input);

        return $this->sendResponse($festival->toArray(), 'Festival saved successfully');
    }

    /**
     * Display the specified festival.
     * GET|HEAD /festivals/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var festival $festival */
        $festival = $this->festivalRepository->find($id);

        if (empty($festival)) {
            return $this->sendError('Festival not found');
        }

        return $this->sendResponse($festival->toArray(), 'Festival retrieved successfully');
    }

    /**
     * Update the specified festival in storage.
     * PUT/PATCH /festivals/{id}
     *
     * @param int $id
     * @param UpdatefestivalAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatefestivalAPIRequest $request)
    {
        $input = $request->all();

        /** @var festival $festival */
        $festival = $this->festivalRepository->find($id);

        if (empty($festival)) {
            return $this->sendError('Festival not found');
        }

        $festival = $this->festivalRepository->update($input, $id);

        return $this->sendResponse($festival->toArray(), 'festival updated successfully');
    }

    /**
     * Remove the specified festival from storage.
     * DELETE /festivals/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var festival $festival */
        $festival = $this->festivalRepository->find($id);

        if (empty($festival)) {
            return $this->sendError('Festival not found');
        }

        $festival->delete();

        return $this->sendResponse($id, 'Festival deleted successfully');
    }
}
