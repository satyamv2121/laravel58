<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateguestAPIRequest;
use App\Http\Requests\API\UpdateguestAPIRequest;
use App\Models\guest;
use App\Repositories\guestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class guestController
 * @package App\Http\Controllers\API
 */

class guestAPIController extends AppBaseController
{
    /** @var  guestRepository */
    private $guestRepository;

    public function __construct(guestRepository $guestRepo)
    {
        $this->guestRepository = $guestRepo;
    }

    /**
     * Display a listing of the guest.
     * GET|HEAD /guests
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $guests = $this->guestRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($guests->toArray(), 'Guests retrieved successfully');
    }

    /**
     * Store a newly created guest in storage.
     * POST /guests
     *
     * @param CreateguestAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateguestAPIRequest $request)
    {
        $input = $request->all();

        $guest = $this->guestRepository->create($input);

        return $this->sendResponse($guest->toArray(), 'Guest saved successfully');
    }

    /**
     * Display the specified guest.
     * GET|HEAD /guests/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var guest $guest */
        $guest = $this->guestRepository->find($id);

        if (empty($guest)) {
            return $this->sendError('Guest not found');
        }

        return $this->sendResponse($guest->toArray(), 'Guest retrieved successfully');
    }

    /**
     * Update the specified guest in storage.
     * PUT/PATCH /guests/{id}
     *
     * @param int $id
     * @param UpdateguestAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateguestAPIRequest $request)
    {
        $input = $request->all();

        /** @var guest $guest */
        $guest = $this->guestRepository->find($id);

        if (empty($guest)) {
            return $this->sendError('Guest not found');
        }

        $guest = $this->guestRepository->update($input, $id);

        return $this->sendResponse($guest->toArray(), 'guest updated successfully');
    }

    /**
     * Remove the specified guest from storage.
     * DELETE /guests/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var guest $guest */
        $guest = $this->guestRepository->find($id);

        if (empty($guest)) {
            return $this->sendError('Guest not found');
        }

        $guest->delete();

        return $this->sendResponse($id, 'Guest deleted successfully');
    }
}
