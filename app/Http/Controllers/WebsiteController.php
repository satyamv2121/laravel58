<?php

namespace App\Http\Controllers;

use App\Models\appmenu;
use Request;
use mysql_xdevapi\Exception;

class WebsiteController extends Controller
{
    public function index()
    {






        $menus=$this->app_menu();
        return view('welcome',compact('menus'));
    }
    public function get_my_music($menu)
    {

        $menus=$this->app_menu();
        $mymenu=appmenu::where('link',$menu)->first();
        $get_menu=Request::segment(1);
        if($get_menu=="top-rate") {
            $my_play_music = '
            [
                    {
                        "name": "Risin\' High (feat Raashan Ahmad)",
                        "artist": "Ancient Astronauts",
                        "album": "We Are to Answer",
                        "url": "https://521dimensions.com/song/Ancient Astronauts - Risin\' High (feat Raashan Ahmad).mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/we-are-to-answer.jpg"
                    },
                    {
                        "name": "The Gun Satyam",
                        "artist": "Lorn",
                        "album": "Ask The Dust",
                        "url": "https://521dimensions.com/song/08 The Gun.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/ask-the-dust.jpg"
                    },
                    {
                        "name": "Anvil",
                        "artist": "Lorn",
                        "album": "Anvil",
                        "url": "https://521dimensions.com/song/LORN - ANVIL.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/anvil.jpg"
                    },
                    {
                        "name": "I Came Running",
                        "artist": "Ancient Astronauts",
                        "album": "We Are to Answer",
                        "url": "https://521dimensions.com/song/ICameRunning-AncientAstronauts.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/we-are-to-answer.jpg"
                    },
                    {
                        "name": "First Snow",
                        "artist": "Emancipator",
                        "album": "Soon It Will Be Cold Enough",
                        "url": "https://521dimensions.com/song/FirstSnow-Emancipator.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/soon-it-will-be-cold-enough.jpg"
                    },
                    {
                        "name": "Terrain",
                        "artist": "pg.lost",
                        "album": "Key",
                        "url": "https://521dimensions.com/song/Terrain-pglost.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/key.jpg"
                    },
                    {
                        "name": "Vorel",
                        "artist": "Russian Circles",
                        "album": "Guidance",
                        "url": "https://521dimensions.com/song/Vorel-RussianCircles.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/guidance.jpg"
                    },
                    {
                        "name": "Intro / Sweet Glory",
                        "artist": "Jimkata",
                        "album": "Die Digital",
                        "url": "https://521dimensions.com/song/IntroSweetGlory-Jimkata.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/die-digital.jpg"
                    },
                    {
                        "name": "Offcut #6",
                        "artist": "Little People",
                        "album": "We Are But Hunks of Wood Remixes",
                        "url": "https://521dimensions.com/song/Offcut6-LittlePeople.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/we-are-but-hunks-of-wood.jpg"
                    },
                    {
                        "name": "Dusk To Dawn",
                        "artist": "Emancipator",
                        "album": "Dusk To Dawn",
                        "url": "https://521dimensions.com/song/DuskToDawn-Emancipator.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/from-dusk-to-dawn.jpg"
                    },
                    {
                        "name": "Anthem",
                        "artist": "Emancipator",
                        "album": "Soon It Will Be Cold Enough",
                        "url": "https://521dimensions.com/song/Anthem-Emancipator.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/soon-it-will-be-cold-enough.jpg"
                    }
                
            ]';
        }
        if($get_menu=="top-play")
        {
            $my_play_music='
            [
              {
                    "name": "The Gun Satyam",
                    "artist": "Lorn",
                    "album": "Ask The Dust",
                    "url": "https://521dimensions.com/song/08 The Gun.mp3",
                    "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/ask-the-dust.jpg"
                },
                {
                    "name": "Risin\' High (feat Raashan Ahmad)",
                    "artist": "Ancient Astronauts",
                    "album": "We Are to Answer",
                    "url": "https://521dimensions.com/song/Ancient Astronauts - Risin\' High (feat Raashan Ahmad).mp3",
                    "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/we-are-to-answer.jpg"
                },
              
                {
                    "name": "Anvil",
                    "artist": "Lorn",
                    "album": "Anvil",
                    "url": "https://521dimensions.com/song/LORN - ANVIL.mp3",
                    "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/anvil.jpg"
                } 
            ]';
        }

        return view('website.my_music_list',compact('menus','mymenu','get_menu','my_play_music'));
    }
    public  function get_song($menu_data)
    {
        if($menu_data=="top-rate") {
            $my_play_music = '
            [
                    {
                        "name": "Risin\' High (feat Raashan Ahmad)",
                        "artist": "Ancient Astronauts",
                        "album": "We Are to Answer",
                        "url": "https://521dimensions.com/song/Ancient Astronauts - Risin\' High (feat Raashan Ahmad).mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/we-are-to-answer.jpg"
                    },
                    {
                        "name": "The Gun Satyam",
                        "artist": "Lorn",
                        "album": "Ask The Dust",
                        "url": "https://521dimensions.com/song/08 The Gun.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/ask-the-dust.jpg"
                    },
                    {
                        "name": "Anvil",
                        "artist": "Lorn",
                        "album": "Anvil",
                        "url": "https://521dimensions.com/song/LORN - ANVIL.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/anvil.jpg"
                    },
                    {
                        "name": "I Came Running",
                        "artist": "Ancient Astronauts",
                        "album": "We Are to Answer",
                        "url": "https://521dimensions.com/song/ICameRunning-AncientAstronauts.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/we-are-to-answer.jpg"
                    },
                    {
                        "name": "First Snow",
                        "artist": "Emancipator",
                        "album": "Soon It Will Be Cold Enough",
                        "url": "https://521dimensions.com/song/FirstSnow-Emancipator.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/soon-it-will-be-cold-enough.jpg"
                    },
                    {
                        "name": "Terrain",
                        "artist": "pg.lost",
                        "album": "Key",
                        "url": "https://521dimensions.com/song/Terrain-pglost.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/key.jpg"
                    },
                    {
                        "name": "Vorel",
                        "artist": "Russian Circles",
                        "album": "Guidance",
                        "url": "https://521dimensions.com/song/Vorel-RussianCircles.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/guidance.jpg"
                    },
                    {
                        "name": "Intro / Sweet Glory",
                        "artist": "Jimkata",
                        "album": "Die Digital",
                        "url": "https://521dimensions.com/song/IntroSweetGlory-Jimkata.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/die-digital.jpg"
                    },
                    {
                        "name": "Offcut #6",
                        "artist": "Little People",
                        "album": "We Are But Hunks of Wood Remixes",
                        "url": "https://521dimensions.com/song/Offcut6-LittlePeople.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/we-are-but-hunks-of-wood.jpg"
                    },
                    {
                        "name": "Dusk To Dawn",
                        "artist": "Emancipator",
                        "album": "Dusk To Dawn",
                        "url": "https://521dimensions.com/song/DuskToDawn-Emancipator.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/from-dusk-to-dawn.jpg"
                    },
                    {
                        "name": "Anthem",
                        "artist": "Emancipator",
                        "album": "Soon It Will Be Cold Enough",
                        "url": "https://521dimensions.com/song/Anthem-Emancipator.mp3",
                        "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/soon-it-will-be-cold-enough.jpg"
                    }
                
            ]';
        }
        if($menu_data=="top-play")
        {
            $my_play_music='
            [
              {
                    "name": "The Gun Satyam",
                    "artist": "Lorn",
                    "album": "Ask The Dust",
                    "url": "https://521dimensions.com/song/08 The Gun.mp3",
                    "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/ask-the-dust.jpg"
                },
                {
                    "name": "Risin\' High (feat Raashan Ahmad)",
                    "artist": "Ancient Astronauts",
                    "album": "We Are to Answer",
                    "url": "https://521dimensions.com/song/Ancient Astronauts - Risin\' High (feat Raashan Ahmad).mp3",
                    "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/we-are-to-answer.jpg"
                },
              
                {
                    "name": "Anvil",
                    "artist": "Lorn",
                    "album": "Anvil",
                    "url": "https://521dimensions.com/song/LORN - ANVIL.mp3",
                    "cover_art_url": "https://521dimensions.com/img/open-source/amplitudejs/album-art/anvil.jpg"
                } 
            ]';
        }
        return $my_play_music;
    }

    public function app_menu()
    {
        return appmenu::where('status','Unblock')->orderBy('position')->get();
    }

}
