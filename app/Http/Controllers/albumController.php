<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatealbumRequest;
use App\Http\Requests\UpdatealbumRequest;
use App\Repositories\albumRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class albumController extends AppBaseController
{
    /** @var  albumRepository */
    private $albumRepository;

    public function __construct(albumRepository $albumRepo)
    {
        $this->albumRepository = $albumRepo;
    }

    /**
     * Display a listing of the album.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $albums = $this->albumRepository->all();

        return view('albums.index')
            ->with('albums', $albums);
    }

    /**
     * Show the form for creating a new album.
     *
     * @return Response
     */
    public function create()
    {
        return view('albums.create');
    }

    /**
     * Store a newly created album in storage.
     *
     * @param CreatealbumRequest $request
     *
     * @return Response
     */
    public function store(CreatealbumRequest $request)
    {
        $input = $request->all();

        $album = $this->albumRepository->create($input);

        Flash::success('Album saved successfully.');

        return redirect(route('albums.index'));
    }

    /**
     * Display the specified album.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $album = $this->albumRepository->find($id);

        if (empty($album)) {
            Flash::error('Album not found');

            return redirect(route('albums.index'));
        }

        return view('albums.show')->with('album', $album);
    }

    /**
     * Show the form for editing the specified album.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $album = $this->albumRepository->find($id);

        if (empty($album)) {
            Flash::error('Album not found');

            return redirect(route('albums.index'));
        }

        return view('albums.edit')->with('album', $album);
    }

    /**
     * Update the specified album in storage.
     *
     * @param int $id
     * @param UpdatealbumRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatealbumRequest $request)
    {
        $album = $this->albumRepository->find($id);

        if (empty($album)) {
            Flash::error('Album not found');

            return redirect(route('albums.index'));
        }

        $album = $this->albumRepository->update($request->all(), $id);

        Flash::success('Album updated successfully.');

        return redirect(route('albums.index'));
    }

    /**
     * Remove the specified album from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $album = $this->albumRepository->find($id);

        if (empty($album)) {
            Flash::error('Album not found');

            return redirect(route('albums.index'));
        }

        $this->albumRepository->delete($id);

        Flash::success('Album deleted successfully.');

        return redirect(route('albums.index'));
    }
}
