<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateappmenuRequest;
use App\Http\Requests\UpdateappmenuRequest;
use App\Repositories\appmenuRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class appmenuController extends AppBaseController
{
    /** @var  appmenuRepository */
    private $appmenuRepository;

    public function __construct(appmenuRepository $appmenuRepo)
    {
        $this->appmenuRepository = $appmenuRepo;
    }

    /**
     * Display a listing of the appmenu.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $appmenus = $this->appmenuRepository->all();

        return view('appmenus.index')
            ->with('appmenus', $appmenus);
    }

    /**
     * Show the form for creating a new appmenu.
     *
     * @return Response
     */
    public function create()
    {
        return view('appmenus.create');
    }

    /**
     * Store a newly created appmenu in storage.
     *
     * @param CreateappmenuRequest $request
     *
     * @return Response
     */
    public function store(CreateappmenuRequest $request)
    {
        $input = $request->all();

        $appmenu = $this->appmenuRepository->create($input);

        Flash::success('Appmenu saved successfully.');

        return redirect(route('appmenus.index'));
    }

    /**
     * Display the specified appmenu.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $appmenu = $this->appmenuRepository->find($id);

        if (empty($appmenu)) {
            Flash::error('Appmenu not found');

            return redirect(route('appmenus.index'));
        }

        return view('appmenus.show')->with('appmenu', $appmenu);
    }

    /**
     * Show the form for editing the specified appmenu.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $appmenu = $this->appmenuRepository->find($id);

        if (empty($appmenu)) {
            Flash::error('Appmenu not found');

            return redirect(route('appmenus.index'));
        }

        return view('appmenus.edit')->with('appmenu', $appmenu);
    }

    /**
     * Update the specified appmenu in storage.
     *
     * @param int $id
     * @param UpdateappmenuRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateappmenuRequest $request)
    {
        $appmenu = $this->appmenuRepository->find($id);

        if (empty($appmenu)) {
            Flash::error('Appmenu not found');

            return redirect(route('appmenus.index'));
        }

        $appmenu = $this->appmenuRepository->update($request->all(), $id);

        Flash::success('Appmenu updated successfully.');

        return redirect(route('appmenus.index'));
    }

    /**
     * Remove the specified appmenu from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $appmenu = $this->appmenuRepository->find($id);

        if (empty($appmenu)) {
            Flash::error('Appmenu not found');

            return redirect(route('appmenus.index'));
        }

        $this->appmenuRepository->delete($id);

        Flash::success('Appmenu deleted successfully.');

        return redirect(route('appmenus.index'));
    }
}
