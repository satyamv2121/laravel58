<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateartistRequest;
use App\Http\Requests\UpdateartistRequest;
use App\Repositories\artistRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class artistController extends AppBaseController
{
    /** @var  artistRepository */
    private $artistRepository;

    public function __construct(artistRepository $artistRepo)
    {
        $this->artistRepository = $artistRepo;
    }

    /**
     * Display a listing of the artist.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $artists = $this->artistRepository->all();

        return view('artists.index')
            ->with('artists', $artists);
    }

    /**
     * Show the form for creating a new artist.
     *
     * @return Response
     */
    public function create()
    {
        return view('artists.create');
    }

    /**
     * Store a newly created artist in storage.
     *
     * @param CreateartistRequest $request
     *
     * @return Response
     */
    public function store(CreateartistRequest $request)
    {
        $input = $request->all();

        $artist = $this->artistRepository->create($input);

        Flash::success('Artist saved successfully.');

        return redirect(route('artists.index'));
    }

    /**
     * Display the specified artist.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $artist = $this->artistRepository->find($id);

        if (empty($artist)) {
            Flash::error('Artist not found');

            return redirect(route('artists.index'));
        }

        return view('artists.show')->with('artist', $artist);
    }

    /**
     * Show the form for editing the specified artist.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $artist = $this->artistRepository->find($id);

        if (empty($artist)) {
            Flash::error('Artist not found');

            return redirect(route('artists.index'));
        }

        return view('artists.edit')->with('artist', $artist);
    }

    /**
     * Update the specified artist in storage.
     *
     * @param int $id
     * @param UpdateartistRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateartistRequest $request)
    {
        $artist = $this->artistRepository->find($id);

        if (empty($artist)) {
            Flash::error('Artist not found');

            return redirect(route('artists.index'));
        }

        $artist = $this->artistRepository->update($request->all(), $id);

        Flash::success('Artist updated successfully.');

        return redirect(route('artists.index'));
    }

    /**
     * Remove the specified artist from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $artist = $this->artistRepository->find($id);

        if (empty($artist)) {
            Flash::error('Artist not found');

            return redirect(route('artists.index'));
        }

        $this->artistRepository->delete($id);

        Flash::success('Artist deleted successfully.');

        return redirect(route('artists.index'));
    }
}
