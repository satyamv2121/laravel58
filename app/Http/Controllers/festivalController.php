<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatefestivalRequest;
use App\Http\Requests\UpdatefestivalRequest;
use App\Repositories\festivalRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class festivalController extends AppBaseController
{
    /** @var  festivalRepository */
    private $festivalRepository;

    public function __construct(festivalRepository $festivalRepo)
    {
        $this->festivalRepository = $festivalRepo;
    }

    /**
     * Display a listing of the festival.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $festivals = $this->festivalRepository->all();

        return view('festivals.index')
            ->with('festivals', $festivals);
    }

    /**
     * Show the form for creating a new festival.
     *
     * @return Response
     */
    public function create()
    {
        return view('festivals.create');
    }

    /**
     * Store a newly created festival in storage.
     *
     * @param CreatefestivalRequest $request
     *
     * @return Response
     */
    public function store(CreatefestivalRequest $request)
    {
        $input = $request->all();

        $festival = $this->festivalRepository->create($input);

        Flash::success('Festival saved successfully.');

        return redirect(route('festivals.index'));
    }

    /**
     * Display the specified festival.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $festival = $this->festivalRepository->find($id);

        if (empty($festival)) {
            Flash::error('Festival not found');

            return redirect(route('festivals.index'));
        }

        return view('festivals.show')->with('festival', $festival);
    }

    /**
     * Show the form for editing the specified festival.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $festival = $this->festivalRepository->find($id);

        if (empty($festival)) {
            Flash::error('Festival not found');

            return redirect(route('festivals.index'));
        }

        return view('festivals.edit')->with('festival', $festival);
    }

    /**
     * Update the specified festival in storage.
     *
     * @param int $id
     * @param UpdatefestivalRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatefestivalRequest $request)
    {
        $festival = $this->festivalRepository->find($id);

        if (empty($festival)) {
            Flash::error('Festival not found');

            return redirect(route('festivals.index'));
        }

        $festival = $this->festivalRepository->update($request->all(), $id);

        Flash::success('Festival updated successfully.');

        return redirect(route('festivals.index'));
    }

    /**
     * Remove the specified festival from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $festival = $this->festivalRepository->find($id);

        if (empty($festival)) {
            Flash::error('Festival not found');

            return redirect(route('festivals.index'));
        }

        $this->festivalRepository->delete($id);

        Flash::success('Festival deleted successfully.');

        return redirect(route('festivals.index'));
    }
}
