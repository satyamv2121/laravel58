<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class album
 * @package App\Models
 * @version October 23, 2019, 12:02 pm UTC
 *
 * @property string poster
 * @property string title
 * @property string release_date
 * @property string viewers
 */
class album extends Model
{
    use SoftDeletes;

    public $table = 'albums';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'poster',
        'title',
        'release_date',
        'viewers'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'poster' => 'string',
        'title' => 'string',
        'release_date' => 'string',
        'viewers' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'poster' => 'required',
        'title' => 'required',
        'release_date' => 'required'
    ];

    
}
