<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class appmenu
 * @package App\Models
 * @version November 7, 2019, 5:53 am UTC
 *
 * @property string name
 * @property string icon
 * @property string link
 * @property string query
 * @property string status
 * @property string position
 * @property string tag
 * @property string pagination
 */
class appmenu extends Model
{
    use SoftDeletes;

    public $table = 'appmenus';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'icon',
        'link',
        'query',
        'status',
        'position',
        'tag',
        'pagination'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'icon' => 'string',
        'link' => 'string',
        'query' => 'string',
        'status' => 'string',
        'position' => 'string',
        'tag' => 'string',
        'pagination' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'icon' => 'required',
        'link' => 'required'
    ];

    
}
