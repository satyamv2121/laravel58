<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class artist
 * @package App\Models
 * @version October 23, 2019, 11:54 am UTC
 *
 * @property string image
 * @property string name
 * @property string status
 */
class artist extends Model
{
    use SoftDeletes;

    public $table = 'artists';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'image',
        'name',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'image' => 'string',
        'name' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'image' => 'required',
        'name' => 'required',
        'status' => 'required'
    ];

    
}
