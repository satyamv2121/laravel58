<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class festival
 * @package App\Models
 * @version October 23, 2019, 11:45 am UTC
 *
 * @property string name
 * @property string poster
 * @property string position
 * @property string status
 */
class festival extends Model
{
    use SoftDeletes;

    public $table = 'festivals';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'poster',
        'position',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'poster' => 'string',
        'position' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'poster' => 'required',
        'position' => 'required',
        'status' => 'required'
    ];

    
}
