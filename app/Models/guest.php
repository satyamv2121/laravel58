<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class guest
 * @package App\Models
 * @version October 23, 2019, 11:04 am UTC
 *
 * @property string company_name
 * @property string logo
 * @property string email
 * @property string password
 * @property string status
 */
class guest extends Model
{
    use SoftDeletes;

    public $table = 'guests';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'company_name',
        'logo',
        'email',
        'password',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'company_name' => 'string',
        'logo' => 'string',
        'email' => 'string',
        'password' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'company_name' => 'required',
        'email' => 'required',
        'password' => 'required',
        'status' => 'required'
    ];

    
}
