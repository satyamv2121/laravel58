<?php

namespace App\Repositories;

use App\Models\album;
use App\Repositories\BaseRepository;

/**
 * Class albumRepository
 * @package App\Repositories
 * @version October 23, 2019, 12:02 pm UTC
*/

class albumRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'poster',
        'title',
        'release_date',
        'viewers'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return album::class;
    }
}
