<?php

namespace App\Repositories;

use App\Models\appmenu;
use App\Repositories\BaseRepository;

/**
 * Class appmenuRepository
 * @package App\Repositories
 * @version November 7, 2019, 5:53 am UTC
*/

class appmenuRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'icon',
        'link',
        'query',
        'status',
        'position',
        'tag',
        'pagination'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return appmenu::class;
    }
}
