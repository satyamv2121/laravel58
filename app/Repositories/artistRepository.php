<?php

namespace App\Repositories;

use App\Models\artist;
use App\Repositories\BaseRepository;

/**
 * Class artistRepository
 * @package App\Repositories
 * @version October 23, 2019, 11:54 am UTC
*/

class artistRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'image',
        'name',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return artist::class;
    }
}
