<?php

namespace App\Repositories;

use App\Models\festival;
use App\Repositories\BaseRepository;

/**
 * Class festivalRepository
 * @package App\Repositories
 * @version October 23, 2019, 11:45 am UTC
*/

class festivalRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'poster',
        'position',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return festival::class;
    }
}
