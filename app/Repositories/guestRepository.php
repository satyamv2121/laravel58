<?php

namespace App\Repositories;

use App\Models\guest;
use App\Repositories\BaseRepository;

/**
 * Class guestRepository
 * @package App\Repositories
 * @version October 23, 2019, 11:04 am UTC
*/

class guestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'company_name',
        'logo',
        'email',
        'password',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return guest::class;
    }
}
