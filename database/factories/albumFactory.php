<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\album;
use Faker\Generator as Faker;

$factory->define(album::class, function (Faker $faker) {

    return [
        'poster' => $faker->word,
        'title' => $faker->word,
        'release_date' => $faker->word,
        'viewers' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
