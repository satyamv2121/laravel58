<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\appmenu;
use Faker\Generator as Faker;

$factory->define(appmenu::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'icon' => $faker->word,
        'link' => $faker->word,
        'query' => $faker->word,
        'status' => $faker->word,
        'position' => $faker->word,
        'tag' => $faker->word,
        'pagination' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
