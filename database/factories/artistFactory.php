<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\artist;
use Faker\Generator as Faker;

$factory->define(artist::class, function (Faker $faker) {

    return [
        'image' => $faker->word,
        'name' => $faker->word,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
