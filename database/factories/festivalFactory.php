<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\festival;
use Faker\Generator as Faker;

$factory->define(festival::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'poster' => $faker->word,
        'position' => $faker->word,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
