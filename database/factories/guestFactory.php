<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\guest;
use Faker\Generator as Faker;

$factory->define(guest::class, function (Faker $faker) {

    return [
        'company_name' => $faker->word,
        'logo' => $faker->word,
        'email' => $faker->word,
        'password' => $faker->word,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
