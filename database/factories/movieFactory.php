<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\movie;
use Faker\Generator as Faker;

$factory->define(movie::class, function (Faker $faker) {

    return [
        'poster_image' => $faker->word,
        'title' => $faker->word,
        'release_date' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
