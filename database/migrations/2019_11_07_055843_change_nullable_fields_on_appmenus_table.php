<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNullableFieldsOnAppmenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appmenus', function (Blueprint $table) {

            $table->string('query')->nullable()->change();
            $table->string('status')->nullable()->default("Block")->change();
            $table->string('position')->nullable()->change();
            $table->string('tag')->nullable()->change();
            $table->string('pagination')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appmenus', function (Blueprint $table) {

        });
    }
}
