<!-- Poster Field -->
<div class="form-group col-sm-6">
    {!! Form::label('poster', 'Poster:') !!}
    {!! Form::file('poster') !!}
</div>
<div class="clearfix"></div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Release Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('release_date', 'Release Date:') !!}
    {!! Form::date('release_date', null, ['class' => 'form-control','id'=>'release_date']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#release_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Viewers Field -->
<div class="form-group col-sm-6">
    {!! Form::label('viewers', 'Viewers:') !!}
    {!! Form::text('viewers', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('albums.index') !!}" class="btn btn-default">Cancel</a>
</div>
