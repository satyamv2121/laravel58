<!-- Poster Field -->
<div class="form-group">
    {!! Form::label('poster', 'Poster:') !!}
    <p>{!! $album->poster !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $album->title !!}</p>
</div>

<!-- Release Date Field -->
<div class="form-group">
    {!! Form::label('release_date', 'Release Date:') !!}
    <p>{!! $album->release_date !!}</p>
</div>

<!-- Viewers Field -->
<div class="form-group">
    {!! Form::label('viewers', 'Viewers:') !!}
    <p>{!! $album->viewers !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $album->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $album->updated_at !!}</p>
</div>

