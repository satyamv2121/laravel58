@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Appmenu
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($appmenu, ['route' => ['appmenus.update', $appmenu->id], 'method' => 'patch']) !!}

                        @include('appmenus.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection