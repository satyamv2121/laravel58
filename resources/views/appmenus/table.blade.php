<div class="table-responsive">
    <table class="table" id="appmenus-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Icon</th>
        <th>Link</th>
        <th>Query</th>
        <th>Status</th>
        <th>Position</th>
        <th>Tag</th>
        <th>Pagination</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($appmenus as $appmenu)
            <tr>
                <td>{!! $appmenu->name !!}</td>
            <td>{!! $appmenu->icon !!}</td>
            <td>{!! $appmenu->link !!}</td>
            <td>{!! $appmenu->query !!}</td>
            <td>{!! $appmenu->status !!}</td>
            <td>{!! $appmenu->position !!}</td>
            <td>{!! $appmenu->tag !!}</td>
            <td>{!! $appmenu->pagination !!}</td>
                <td>
                    {!! Form::open(['route' => ['appmenus.destroy', $appmenu->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('appmenus.show', [$appmenu->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('appmenus.edit', [$appmenu->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
