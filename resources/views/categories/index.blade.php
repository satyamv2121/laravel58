
@extends('layouts.app')

@section('css')
<style>
        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active {
            border: 1px solid #c5c5c5;
            background: #fff;
            font-weight: normal;
            color: #454545;
        }    
</style>    
@endsection
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Categories</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('categories.create') !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('categories.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection
@section('scripts')
<script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
  integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
  crossorigin="anonymous"></script>

    <script>
        $(document).ready(function(){
            $('table tbody').sortable({
                update:function(event,ui){
                    $(this).children().each(function(index){
                        
                        if($(this).attr('data-position') !=(index+1)){
                            $(this).attr('data-position',(index+1)).addClass('updated')
                        }
                    });
                    saveNewPositions();
                }
            });

        });

        function saveNewPositions()
        {
            var positions=[];
            $('.updated').each(function(){
                positions.push([$(this).attr('data-index'),$(this).attr('data-position')]);
                $(this).removeClass('updated');
            });        
            $.ajax({
                url:"change_my_position",
                type:'post',
                data:{update:1,positions:positions,_token:"{{ csrf_token() }}"},
                success:function(response)
                {
                    console.table(response);
                }

            })
        }
    </script>
@endsection

