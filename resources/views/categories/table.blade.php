<div class="table-responsive">
    <table class="table" id="categories-table">
        <thead>
            <tr>
                <th style="font-size:12px;">Drag&Drop</th>
                <th>Name</th>
                <th>Position</th>
                <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr class="ui-state-default" data-index="{!! $category->id !!}" data-position="{!! $category->position !!}">
                   
            <td width="5px"> <i class="fa fa-arrows-v text-red"></i></td>
            <td>{!! $category->name !!}</td>
            <td>{!! $category->position !!}</td>
            <td >{!! $category->status !!}</td>
                <td>
                    {!! Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('categories.show', [$category->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('categories.edit', [$category->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
