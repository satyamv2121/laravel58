@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Festival
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($festival, ['route' => ['festivals.update', $festival->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('festivals.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection