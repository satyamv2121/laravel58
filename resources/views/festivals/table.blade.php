<div class="table-responsive">
    <table class="table" id="festivals-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Poster</th>
        <th>Position</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($festivals as $festival)
            <tr>
                <td>{!! $festival->name !!}</td>
            <td>{!! $festival->poster !!}</td>
            <td>{!! $festival->position !!}</td>
            <td>{!! $festival->status !!}</td>
                <td>
                    {!! Form::open(['route' => ['festivals.destroy', $festival->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('festivals.show', [$festival->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('festivals.edit', [$festival->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
