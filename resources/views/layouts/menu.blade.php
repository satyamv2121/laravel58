<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{!! route('categories.index') !!}"><i class="fa fa-edit"></i><span>Categories</span></a>
</li>





<li class="{{ Request::is('guests*') ? 'active' : '' }}">
    <a href="{!! route('guests.index') !!}"><i class="fa fa-edit"></i><span>Guests</span></a>
</li>

<li class="{{ Request::is('festivals*') ? 'active' : '' }}">
    <a href="{!! route('festivals.index') !!}"><i class="fa fa-edit"></i><span>Festivals</span></a>
</li>

<li class="{{ Request::is('artists*') ? 'active' : '' }}">
    <a href="{!! route('artists.index') !!}"><i class="fa fa-edit"></i><span>Artists</span></a>
</li>

<li class="{{ Request::is('albums*') ? 'active' : '' }}">
    <a href="{!! route('albums.index') !!}"><i class="fa fa-edit"></i><span>Albums</span></a>
</li>

<li class="{{ Request::is('appmenus*') ? 'active' : '' }}">
    <a href="{!! route('appmenus.index') !!}"><i class="fa fa-edit"></i><span>Appmenus</span></a>
</li>

