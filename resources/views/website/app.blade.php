<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from kri8thm.kiraninfosoft.com/listen/theme/demo/home.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 Oct 2019 07:19:50 GMT -->
<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Seo Meta -->
    <meta name="description" content="Listen App - Online Music Streaming App Template">
    <meta name="keywords" content="music template, music app, music web app, responsive music app, music, themeforest, html music app template, css3, html5">

    <title>Satyaminfo Online Music App</title>

    <!-- Favicon -->
    <link href="{{url('/')}}/public/assets/images/logos/favicon.png" rel="icon"/>

    <!-- IOS Touch Icons -->
    <link rel="apple-touch-icon" href="{{url('/')}}/public/assets/images/logos/touch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{url('/')}}/public/assets/images/logos/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{url('/')}}/public/assets/images/logos/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="167x167" href="{{url('/')}}/public/assets/images/logos/touch-icon-ipad-retina.png">

    <!-- Styles -->
    <link href="{{url('/')}}/public/assets/css/vendors.bundle.css" rel="stylesheet" type="text/css"/>
    <link href="{{url('/')}}/public/assets/css/styles.bundle.css" rel="stylesheet" type="text/css"/>

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">
    <style>
        .fa {
            font-size: 17px;
        }
        .badge-warning {

            display: table;
            top: -6px;
            left: -25px;
            position: absolute;
        }
        .badge-danger {

            display: table;
            top: 25px;
            left: -25px;
            position: absolute;

        }
        .custom-card--labels .badge
        {
            width: 50px;
            height: 25px;
            border-radius: 0px;
            padding-top:10px;

        }
        .banner:after {
            background: linear-gradient(to bottom, rgba(52, 58, 64, 0) 0%, #ffffff 0%, #ffffff 100%);
        }
        body.theme-dark .banner:after
        {
            background: linear-gradient(to bottom, rgba(52, 58, 64, 0) 0%, #343a40 0%, #343a40 100%) !important;
        }
    /* sidebar */
        .navbar-nav .nav-link span {
            text-align: left;
            width: 192px;
        }
        .audio .song-image img {
            border-top-left-radius: 1rem;
            border-bottom-left-radius: 1rem;
            height: unset !important;
        }

    </style>
    @yield('css')
</head>
<body>

<!-- Begin | Loading [[ Find at scss/framework/base/loader/loader.scss ]] -->
<div id="loading">
    <div class="loader">
        <div class="eq">
            <span class="eq-bar eq-bar--1"></span>
            <span class="eq-bar eq-bar--2"></span>
            <span class="eq-bar eq-bar--3"></span>
            <span class="eq-bar eq-bar--4"></span>
            <span class="eq-bar eq-bar--5"></span>
            <span class="eq-bar eq-bar--6"></span>
        </div>
        <span class="text">Loading</span>
    </div>
</div>
<!-- End | Loading -->

    @include('website.sidebar')

    @yield('container')


</div>
<!-- End | Wrapper -->
<!-- Begin | Language Modal -->
<div class="modal fade" id="lang" tabindex="-1" role="dialog" aria-labelledby="langTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title mb-1" id="langTitle">Language</h5>
                    <p class="text-muted">Please select the language(s) of the music you listen to.</p>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group row">
                    <li class="list-group-item border-0 col-sm-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ch1" checked>
                            <label class="custom-control-label" for="ch1">Hindi</label>
                        </div>
                    </li>
                    <li class="list-group-item border-0 col-sm-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ch2">
                            <label class="custom-control-label" for="ch2">Punjabi</label>
                        </div>
                    </li>
                    <li class="list-group-item border-0 col-sm-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ch3">
                            <label class="custom-control-label" for="ch3">Tamil</label>
                        </div>
                    </li>
                    <li class="list-group-item border-0 col-sm-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ch4">
                            <label class="custom-control-label" for="ch4">Bengali</label>
                        </div>
                    </li>
                    <li class="list-group-item border-0 col-sm-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ch5">
                            <label class="custom-control-label" for="ch5">Kannada</label>
                        </div>
                    </li>
                    <li class="list-group-item border-0 col-sm-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ch6">
                            <label class="custom-control-label" for="ch6">Gujarati</label>
                        </div>
                    </li>
                    <li class="list-group-item border-0 col-sm-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ch7">
                            <label class="custom-control-label" for="ch7">Urdu</label>
                        </div>
                    </li>
                    <li class="list-group-item border-0 col-sm-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ch8">
                            <label class="custom-control-label" for="ch8">Rajasthani</label>
                        </div>
                    </li>
                    <li class="list-group-item border-0 col-sm-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ch9" checked>
                            <label class="custom-control-label" for="ch9">English</label>
                        </div>
                    </li>
                    <li class="list-group-item border-0 col-sm-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ch10">
                            <label class="custom-control-label" for="ch10">Telugu</label>
                        </div>
                    </li>
                    <li class="list-group-item border-0 col-sm-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ch11">
                            <label class="custom-control-label" for="ch11">Bhojpuri</label>
                        </div>
                    </li>
                    <li class="list-group-item border-0 col-sm-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ch12">
                            <label class="custom-control-label" for="ch12">Malayalam</label>
                        </div>
                    </li>
                    <li class="list-group-item border-0 col-sm-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ch13">
                            <label class="custom-control-label" for="ch13">Marathi</label>
                        </div>
                    </li>
                    <li class="list-group-item border-0 col-sm-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ch14">
                            <label class="custom-control-label" for="ch14">Haryanvi</label>
                        </div>
                    </li>
                    <li class="list-group-item border-0 col-sm-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ch15">
                            <label class="custom-control-label" for="ch15">Assamese</label>
                        </div>
                    </li>
                    <li class="list-group-item border-0 col-sm-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ch16">
                            <label class="custom-control-label" for="ch16">Odia</label>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="modal-footer text-center d-block">
                <button type="button" class="btn btn-primary btn-pill" id="langApply">Apply</button>
            </div>
        </div>
    </div>
</div>
<!-- End | Language Modal -->

<!-- Back Drop -->
<div class="backdrop header-backdrop"></div>
<div class="backdrop sidebar-backdrop"></div>




{{--My play music tools--}}
<div id="player-screen" class="d-none">
    <div id="player-progress-bar-container">
        <progress id="song-played-progress" class="amplitude-song-played-progress"></progress>
        <progress id="song-buffered-progress" class="amplitude-buffered-progress" value="0"></progress>
    </div>
</div>

<div id="audioPlayer" class="player-primary">


    <!-- Begin | Audio Player Progress -->
    <div id="progress-container">
        <input type="range" class="amplitude-song-slider">
        <progress class="audio-progress audio-progress--played amplitude-song-played-progress" value="0"></progress>
        <progress class="audio-progress audio-progress--buffered amplitude-buffered-progress" value="1"></progress>
    </div>
    <!-- End | Audio Player Progress -->

    <!-- Begin | Audio -->
    <div class="audio">
        <div class="song-image">
            <img data-amplitude-song-info="cover_art_url" style="height:unset !important" src="" alt="" >
        </div>

        <div class="song-info pl-3">
            <span class="song-name d-inline-block text-truncate" data-amplitude-song-info="name">Where is your letter</span>
            <span class="song-artists d-block text-muted" data-amplitude-song-info="artist">Jina Moore &amp; Lenisa Gory</span>
        </div>
    </div>
    <!-- End | Audio -->

    <!-- Begin | Audio Controls -->
    <div class="audio-controls">
        <div class="audio-controls--left d-flex mr-auto">
            <button class="btn btn-icon-only amplitude-repeat amplitude-repeat-off"><i class="ion-md-sync"></i></button>
        </div>
        <div class="audio-controls--main d-flex">
            <button class="btn btn-icon-only amplitude-prev"><i class="ion-md-skip-backward"></i></button>
            <button class="btn btn-air btn-pill btn-default btn-icon-only amplitude-play-pause amplitude-paused">
                <i class="ion-md-play"></i>
                <i class="ion-md-pause"></i>
            </button>
            <button class="btn btn-icon-only amplitude-next"><i class="ion-md-skip-forward"></i></button>
        </div>

        <div class="audio-controls--right d-flex ml-auto">
            <button class="btn btn-icon-only amplitude-shuffle amplitude-shuffle-off"><i class="ion-md-shuffle"></i></button>
        </div>
    </div>
    <!-- End | Audio Controls -->

    <!-- Begin | Audio Info -->
    <div class="audio-info d-flex align-items-center pr-4">
                    <span class="mr-4">
                        <span class="amplitude-current-minutes">00</span>:<span class="amplitude-current-seconds">00</span> /
                        <span class="amplitude-duration-minutes">00</span>:<span class="amplitude-duration-seconds">30</span>
                    </span>
        <div class="audio-volume dropdown">
            <button class="btn btn-icon-only" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ion-md-volume-high"></i></button>
            <div class="dropdown-menu dropdown-menu-right volume-dropdown-menu" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 5px, 0px);" x-out-of-boundaries="">
                <input type="range" class="amplitude-volume-slider" value="100">
            </div>
        </div>

        <div class="dropleft">
            <button class="btn btn-icon-only" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v"></i>
            </button>
            <ul class="dropdown-menu" x-placement="right-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(5px, 0px, 0px);" x-out-of-boundaries="">
                <li class="dropdown-item">
                    <a href="javascript:void(0);" class="dropdown-link">
                        <i class="fa fa-heart-o"></i> <span>Favorite</span>
                    </a>
                </li>
                <li class="dropdown-item">
                    <a href="javascript:void(0);" class="dropdown-link">
                        <i class="fa fa-plus"></i> <span>Add to Playlist</span>
                    </a>
                </li>
                <li class="dropdown-item">
                    <a href="javascript:void(0);" class="dropdown-link">
                        <i class="fa fa-download"></i> <span>Download</span>
                    </a>
                </li>
                <li class="dropdown-item">
                    <a href="javascript:void(0);" class="dropdown-link">
                        <i class="fa fa-share-alt"></i> <span>Share</span>
                    </a>
                </li>
                <li class="dropdown-item">
                    <a href="song-details.html" class="dropdown-link">
                        <i class="fa fa-info-circle"></i>
                        <span>Song Info</span>
                    </a>
                </li>
            </ul>
        </div>
        <button class="btn btn-icon-only" id="playList"><i class="ion-md-musical-note"></i></button>
    </div>
    <!-- End | Audio Info -->
</div>
{{--My play music tools end--}}

<!-- Scripts -->
<script src="{{url('/')}}/public/assets/js/vendors.bundle.js"></script>
<script src="{{url('/')}}/public/assets/js/scripts.bundle.js"></script>
@php
$menu="#artist,#hello";
@endphp
<script>
    $('.nav-link').click(function(){
        $('.nav-link').removeClass('active');
        $(this).addClass('active');
    });
    {{--$('{{$menu}}').click(function(){--}}
    {{--    $.ajax({--}}
    {{--        url:"{{url('/')}}/artist",--}}
    {{--        type:"get",--}}
    {{--        success:function (html_data) {--}}
    {{--          console.log(html_data);--}}
    {{--        }--}}
    {{--    })--}}
    {{--})--}}
</script>
@yield('scripts')
</body>

<!-- Mirrored from kri8thm.kiraninfosoft.com/listen/theme/demo/home.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 Oct 2019 07:20:58 GMT -->
</html>
