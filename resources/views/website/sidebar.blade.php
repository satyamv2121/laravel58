
<!-- Begin | Wrapper [[ Find at scss/framework/base/wrapper/wrapper.scss ]] -->
<div id="wrapper" data-scrollable="true">

    <!-- Begin | Sidebar [[ Find at scss/framework/base/sidebar/left/sidebar.scss ]] -->
    <aside id="sidebar" class="sidebar-primary">

        <!-- Begin | Sidebar Header -->
        <div class="sidebar-header d-flex align-items-center">
            <a href="http://kri8thm.kiraninfosoft.com/listen/theme/index.html" class="brand">
                <img src="http://kri8thm.kiraninfosoft.com/listen/theme/assets/images/logos/logo.svg" alt="listen-app">
            </a>

            <button type="button" class="btn p-0 ml-auto" id="hideSidebar">
                <i class="ion-md-arrow-back h3"></i>
            </button>

            <button type="button" class="btn toggle-menu" id="toggleSidebar">
                <span></span>
                <span></span>
                <span></span>
            </button>
        </div>
        <!-- End | Sidebar Header -->

        <!-- Begin | Navbar [[ Find at scss/framework/components/navbar/navbar.scss ]] -->
        <nav class="navbar">
            <ul class="navbar-nav" data-scrollable="true">
                <li class="nav-item nav-header">Browse Music</li>
                <li class="nav-item">
                    <a href="{{url('/')}}" class="nav-link {{Request::segment(1)==""?"active":""}}"><i class="fa fa-home"></i><span>Home</span></a>
                </li>
                @foreach($menus as $menu)
                <li class="nav-item">
{{--                    <a href="{{url('/')}}" class="nav-link active"><i class="fa fa-home"></i><span>Home</span></a>--}}
                    <a href="{{url('/').'/'.$menu->link}}" class="nav-link {{Request::segment(1)==$menu->link?"active":""}}" id="{{$menu->name}}"><i class="fa fa-{{$menu->icon}}"></i><span>{{$menu->name}} Music <b class="badge badge-pill badge-{{$menu->tag=="New"? "success":"info"}}">{{$menu->tag!=""? $menu->tag :""}}</b></span></a>
                </li>
                @endforeach

                <li class="nav-item nav-header">Your Music</li>
                <li class="nav-item">
                    <a href="favorites.html" class="nav-link"><i class="fa fa-heart-o"></i><span>Favorites</span></a>
                </li>
                <li class="nav-item">
                    <a href="history.html" class="nav-link"><i class="fa fa-history"></i><span>History</span></a>
                </li>
                {{--
                Future me
                    <li class="nav-item nav-header">Music Events update version</li>
                    <li class="nav-item">
                        <a href="events.html" class="nav-link"><i class="fa fa-calendar"></i><span>Events</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="event-details.html" class="nav-link"><i class="fa fa-newspaper-o"></i><span>Event Details</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="add-event.html" class="nav-link"><i class="fa fa-pencil-square-o"></i><span>Add Event</span></a>
                    </li>
                  --}}
            </ul>
        </nav>
        <!-- End | Navbar -->





        <!-- Begin | Sidebar Footer -->
        {{--  if user is aur client exccess this button manage in admin site     --}}
        <div class="sidebar-footer">
            <a href="add-music.html" class="btn btn-block btn-danger btn-air btn-bold">
                <i class="ion-md-musical-note"></i>
                <span>Manage Music</span>
            </a>
        </div>
        <!-- End | Sidebar Footer -->
    </aside>
    <!-- End | Sidebar -->

    <main id="pageWrapper">

        <!-- Begin | Header [[ Find at scss/framework/base/header/header.scss ]] -->
        <header id="header" class="bg-primary">

            <div class="d-flex align-items-center">

                <button type="button" class="btn toggle-menu mr-3" id="openSidebar">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>

                <form action="#" id="searchForm">

                    <button type="button" class="btn ion-ios-search"></button>
                    <input type="text" placeholder="Search..." id="searchInput" class="form-control">

                    <!-- Begin | Search Card [[ Find at scss/framework/base/search/search.scss ]] -->
                    <div class="search-card" data-scrollable="true">
                        <!-- Begin | Search Result List -->
                        <div class="mb-3">
                            <!-- Begin | Search Result List Header -->
                            <div class="d-flex">
                                <span class="text-uppercase mr-auto font-weight-bold text-dark">Artists</span>
                                <a href="artists.html">View All</a>
                            </div>
                            <!-- End | Search Result List Header -->
                            <hr>
                            <!-- Begin | Result List -->
                            <div class="row">
                                <div class="col-xl-2 col-md-4 col-6">
                                    <div class="custom-card mb-3">
                                        <a href="artist-details.html" class="text-dark">
                                            <img src="{{url('/')}}/public/assets/images/cover/medium/1.jpg" alt="" class="card-img--radius-md">
                                            <p class="text-truncate mt-2">Arebica Luna</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xl-2 col-md-4 col-6">
                                    <div class="custom-card mb-3">
                                        <a href="artist-details.html" class="text-dark">
                                            <img src="{{url('/')}}/public/assets/images/cover/medium/2.jpg" alt="" class="card-img--radius-md">
                                            <p class="text-truncate mt-2">Gerrina Linda</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xl-2 col-md-4 col-6">
                                    <div class="custom-card mb-3">
                                        <a href="artist-details.html" class="text-dark">
                                            <img src="{{url('/')}}/public/assets/images/cover/medium/3.jpg" alt="" class="card-img--radius-md">
                                            <p class="text-truncate mt-2">Zunira Willy</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xl-2 col-md-4 col-6">
                                    <div class="custom-card mb-3">
                                        <a href="artist-details.html" class="text-dark">
                                            <img src="{{url('/')}}/public/assets/images/cover/medium/4.jpg" alt="" class="card-img--radius-md">
                                            <p class="text-truncate mt-2">Johnny Marro</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xl-2 col-md-4 col-6">
                                    <div class="custom-card mb-3">
                                        <a href="artist-details.html" class="text-dark">
                                            <img src="{{url('/')}}/public/assets/images/cover/medium/5.jpg" alt="" class="card-img--radius-md">
                                            <p class="text-truncate mt-2">Jina Moore</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xl-2 col-md-4 col-6">
                                    <div class="custom-card mb-3">
                                        <a href="artist-details.html" class="text-dark">
                                            <img src="{{url('/')}}/public/assets/images/cover/medium/6.jpg" alt="" class="card-img--radius-md">
                                            <p class="text-truncate mt-2">Rasomi Pelina</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- End | Result List -->
                        </div>
                        <!-- End | Search Result List -->

                        <!-- Begin | Search Result List -->
                        <div class="mb-3">
                            <!-- Begin | Search Result List Header -->
                            <div class="d-flex">
                                <span class="text-uppercase mr-auto font-weight-bold text-dark">Track</span>
                                <a href="songs.html">View All</a>
                            </div>
                            <!-- End | Search Result List Header -->
                            <hr>
                            <!-- Begin | Result List -->
                            <div class="row">
                                <div class="col-xl-4 col-md-6 col-12">
                                    <div class="custom-card mb-3">
                                        <a href="song-details.html" class="text-dark custom-card--inline">
                                            <div class="custom-card--inline-img">
                                                <img src="{{url('/')}}/public/assets/images/cover/small/1.jpg" alt="" class="card-img--radius-sm">
                                            </div>

                                            <div class="custom-card--inline-desc">
                                                <p class="text-truncate mb-0">I Love You Mummy</p>
                                                <p class="text-truncate text-muted font-sm">Arebica Luna</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-6 col-12">
                                    <div class="custom-card mb-3">
                                        <a href="song-details.html" class="text-dark custom-card--inline">
                                            <div class="custom-card--inline-img">
                                                <img src="{{url('/')}}/public/assets/images/cover/small/2.jpg" alt="" class="card-img--radius-sm">
                                            </div>

                                            <div class="custom-card--inline-desc">
                                                <p class="text-truncate mb-0">Shack your butty</p>
                                                <p class="text-truncate text-muted font-sm">Gerrina Linda</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-6 col-12">
                                    <div class="custom-card mb-3">
                                        <a href="song-details.html" class="text-dark custom-card--inline">
                                            <div class="custom-card--inline-img">
                                                <img src="{{url('/')}}/public/assets/images/cover/small/3.jpg" alt="" class="card-img--radius-sm">
                                            </div>

                                            <div class="custom-card--inline-desc">
                                                <p class="text-truncate mb-0">Do it your way(Female)</p>
                                                <p class="text-truncate text-muted font-sm">Zunira Willy & Nutty Nina</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- End | Result List -->
                        </div>
                        <!-- End | Search Result List -->

                        <!-- Begin | Search Result List -->
                        <div>
                            <!-- Begin | Search Result List Header -->
                            <div class="d-flex">
                                <span class="text-uppercase mr-auto font-weight-bold text-dark">Albums</span>
                                <a href="songs.html">View All</a>
                            </div>
                            <!-- End | Search Result List Header -->
                            <hr>
                            <!-- Begin | Result List -->
                            <div class="row">
                                <div class="col-xl-4 col-md-6 col-12">
                                    <div class="custom-card mb-3">
                                        <a href="song-details.html" class="text-dark custom-card--inline">
                                            <div class="custom-card--inline-img">
                                                <img src="{{url('/')}}/public/assets/images/cover/small/4.jpg" alt="" class="card-img--radius-sm">
                                            </div>

                                            <div class="custom-card--inline-desc">
                                                <p class="text-truncate mb-0">Say yes</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-6 col-12">
                                    <div class="custom-card mb-3">
                                        <a href="song-details.html" class="text-dark custom-card--inline">
                                            <div class="custom-card--inline-img">
                                                <img src="{{url('/')}}/public/assets/images/cover/small/5.jpg" alt="" class="card-img--radius-sm">
                                            </div>

                                            <div class="custom-card--inline-desc">
                                                <p class="text-truncate mb-0">Where is your letter</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-6 col-12">
                                    <div class="custom-card mb-3">
                                        <a href="song-details.html" class="text-dark custom-card--inline">
                                            <div class="custom-card--inline-img">
                                                <img src="{{url('/')}}/public/assets/images/cover/small/6.jpg" alt="" class="card-img--radius-sm">
                                            </div>

                                            <div class="custom-card--inline-desc">
                                                <p class="text-truncate mb-0">Hey not me</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- End | Result List -->
                        </div>
                        <!-- End | Search Result List -->
                    </div>
                    <!-- End | Search Card -->

                </form>

                <!-- Begin | Header Options -->
                <ul class="header-options d-flex align-items-center">
                    <li>
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#lang" class="language">
                            <span>Language</span>
                            <img src="http://kri8thm.kiraninfosoft.com/listen/theme/assets/images/svg/translate.svg" alt="">
                        </a>
                    </li>
                    <li class="dropdown fade-in">
                        <a href="javascript:void(0);" class="d-flex align-items-center py-2" role="button" id="userMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="avatar avatar-sm avatar-circle"><img src="{{url('/')}}/public/assets/images/users/thumb.jpg" alt="user"></div>
                            <span class="pl-2">Halo Admin</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userMenu">
                            <a class="dropdown-item" href="profile.html"><i class="ion-md-contact"></i> <span>Profile</span></a>
                            <a class="dropdown-item" href="plan.html"><i class="ion-md-radio-button-off"></i> <span>Your Plan</span></a>
                            <a class="dropdown-item" href="settings.html"><i class="ion-md-settings"></i> <span>Settings</span></a>
                            <div class="dropdown-divider"></div>
                            <div class="px-4 py-2">
                                <a href="#" class="btn btn-sm btn-air btn-pill btn-danger">Logout</a>
                            </div>
                        </div>
                    </li>
                </ul>
                <!-- End | Header Options -->
            </div>

        </header>
        <!-- End | Header -->




