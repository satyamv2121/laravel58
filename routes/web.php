<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','WebsiteController@index');
Route::get('/get_song/{menu_data}','WebsiteController@get_song');

Route::get('/{menu}','WebsiteController@get_my_music');





Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('categories', 'categoryController');

// change position

Route::post('/change_my_position','categoryController@change_position');










Route::resource('guests', 'guestController');

Route::resource('festivals', 'festivalController');

Route::resource('artists', 'artistController');

Route::resource('albums', 'albumController');


Route::resource('appmenus', 'appmenuController');
