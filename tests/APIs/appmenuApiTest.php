<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\appmenu;

class appmenuApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_appmenu()
    {
        $appmenu = factory(appmenu::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/appmenus', $appmenu
        );

        $this->assertApiResponse($appmenu);
    }

    /**
     * @test
     */
    public function test_read_appmenu()
    {
        $appmenu = factory(appmenu::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/appmenus/'.$appmenu->id
        );

        $this->assertApiResponse($appmenu->toArray());
    }

    /**
     * @test
     */
    public function test_update_appmenu()
    {
        $appmenu = factory(appmenu::class)->create();
        $editedappmenu = factory(appmenu::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/appmenus/'.$appmenu->id,
            $editedappmenu
        );

        $this->assertApiResponse($editedappmenu);
    }

    /**
     * @test
     */
    public function test_delete_appmenu()
    {
        $appmenu = factory(appmenu::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/appmenus/'.$appmenu->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/appmenus/'.$appmenu->id
        );

        $this->response->assertStatus(404);
    }
}
