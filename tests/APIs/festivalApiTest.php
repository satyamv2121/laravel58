<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\festival;

class festivalApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_festival()
    {
        $festival = factory(festival::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/festivals', $festival
        );

        $this->assertApiResponse($festival);
    }

    /**
     * @test
     */
    public function test_read_festival()
    {
        $festival = factory(festival::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/festivals/'.$festival->id
        );

        $this->assertApiResponse($festival->toArray());
    }

    /**
     * @test
     */
    public function test_update_festival()
    {
        $festival = factory(festival::class)->create();
        $editedfestival = factory(festival::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/festivals/'.$festival->id,
            $editedfestival
        );

        $this->assertApiResponse($editedfestival);
    }

    /**
     * @test
     */
    public function test_delete_festival()
    {
        $festival = factory(festival::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/festivals/'.$festival->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/festivals/'.$festival->id
        );

        $this->response->assertStatus(404);
    }
}
