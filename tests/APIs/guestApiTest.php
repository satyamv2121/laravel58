<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\guest;

class guestApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_guest()
    {
        $guest = factory(guest::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/guests', $guest
        );

        $this->assertApiResponse($guest);
    }

    /**
     * @test
     */
    public function test_read_guest()
    {
        $guest = factory(guest::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/guests/'.$guest->id
        );

        $this->assertApiResponse($guest->toArray());
    }

    /**
     * @test
     */
    public function test_update_guest()
    {
        $guest = factory(guest::class)->create();
        $editedguest = factory(guest::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/guests/'.$guest->id,
            $editedguest
        );

        $this->assertApiResponse($editedguest);
    }

    /**
     * @test
     */
    public function test_delete_guest()
    {
        $guest = factory(guest::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/guests/'.$guest->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/guests/'.$guest->id
        );

        $this->response->assertStatus(404);
    }
}
