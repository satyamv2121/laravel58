<?php namespace Tests\Repositories;

use App\Models\album;
use App\Repositories\albumRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class albumRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var albumRepository
     */
    protected $albumRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->albumRepo = \App::make(albumRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_album()
    {
        $album = factory(album::class)->make()->toArray();

        $createdalbum = $this->albumRepo->create($album);

        $createdalbum = $createdalbum->toArray();
        $this->assertArrayHasKey('id', $createdalbum);
        $this->assertNotNull($createdalbum['id'], 'Created album must have id specified');
        $this->assertNotNull(album::find($createdalbum['id']), 'album with given id must be in DB');
        $this->assertModelData($album, $createdalbum);
    }

    /**
     * @test read
     */
    public function test_read_album()
    {
        $album = factory(album::class)->create();

        $dbalbum = $this->albumRepo->find($album->id);

        $dbalbum = $dbalbum->toArray();
        $this->assertModelData($album->toArray(), $dbalbum);
    }

    /**
     * @test update
     */
    public function test_update_album()
    {
        $album = factory(album::class)->create();
        $fakealbum = factory(album::class)->make()->toArray();

        $updatedalbum = $this->albumRepo->update($fakealbum, $album->id);

        $this->assertModelData($fakealbum, $updatedalbum->toArray());
        $dbalbum = $this->albumRepo->find($album->id);
        $this->assertModelData($fakealbum, $dbalbum->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_album()
    {
        $album = factory(album::class)->create();

        $resp = $this->albumRepo->delete($album->id);

        $this->assertTrue($resp);
        $this->assertNull(album::find($album->id), 'album should not exist in DB');
    }
}
