<?php namespace Tests\Repositories;

use App\Models\appmenu;
use App\Repositories\appmenuRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class appmenuRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var appmenuRepository
     */
    protected $appmenuRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->appmenuRepo = \App::make(appmenuRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_appmenu()
    {
        $appmenu = factory(appmenu::class)->make()->toArray();

        $createdappmenu = $this->appmenuRepo->create($appmenu);

        $createdappmenu = $createdappmenu->toArray();
        $this->assertArrayHasKey('id', $createdappmenu);
        $this->assertNotNull($createdappmenu['id'], 'Created appmenu must have id specified');
        $this->assertNotNull(appmenu::find($createdappmenu['id']), 'appmenu with given id must be in DB');
        $this->assertModelData($appmenu, $createdappmenu);
    }

    /**
     * @test read
     */
    public function test_read_appmenu()
    {
        $appmenu = factory(appmenu::class)->create();

        $dbappmenu = $this->appmenuRepo->find($appmenu->id);

        $dbappmenu = $dbappmenu->toArray();
        $this->assertModelData($appmenu->toArray(), $dbappmenu);
    }

    /**
     * @test update
     */
    public function test_update_appmenu()
    {
        $appmenu = factory(appmenu::class)->create();
        $fakeappmenu = factory(appmenu::class)->make()->toArray();

        $updatedappmenu = $this->appmenuRepo->update($fakeappmenu, $appmenu->id);

        $this->assertModelData($fakeappmenu, $updatedappmenu->toArray());
        $dbappmenu = $this->appmenuRepo->find($appmenu->id);
        $this->assertModelData($fakeappmenu, $dbappmenu->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_appmenu()
    {
        $appmenu = factory(appmenu::class)->create();

        $resp = $this->appmenuRepo->delete($appmenu->id);

        $this->assertTrue($resp);
        $this->assertNull(appmenu::find($appmenu->id), 'appmenu should not exist in DB');
    }
}
