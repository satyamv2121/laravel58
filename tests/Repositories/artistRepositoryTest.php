<?php namespace Tests\Repositories;

use App\Models\artist;
use App\Repositories\artistRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class artistRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var artistRepository
     */
    protected $artistRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->artistRepo = \App::make(artistRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_artist()
    {
        $artist = factory(artist::class)->make()->toArray();

        $createdartist = $this->artistRepo->create($artist);

        $createdartist = $createdartist->toArray();
        $this->assertArrayHasKey('id', $createdartist);
        $this->assertNotNull($createdartist['id'], 'Created artist must have id specified');
        $this->assertNotNull(artist::find($createdartist['id']), 'artist with given id must be in DB');
        $this->assertModelData($artist, $createdartist);
    }

    /**
     * @test read
     */
    public function test_read_artist()
    {
        $artist = factory(artist::class)->create();

        $dbartist = $this->artistRepo->find($artist->id);

        $dbartist = $dbartist->toArray();
        $this->assertModelData($artist->toArray(), $dbartist);
    }

    /**
     * @test update
     */
    public function test_update_artist()
    {
        $artist = factory(artist::class)->create();
        $fakeartist = factory(artist::class)->make()->toArray();

        $updatedartist = $this->artistRepo->update($fakeartist, $artist->id);

        $this->assertModelData($fakeartist, $updatedartist->toArray());
        $dbartist = $this->artistRepo->find($artist->id);
        $this->assertModelData($fakeartist, $dbartist->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_artist()
    {
        $artist = factory(artist::class)->create();

        $resp = $this->artistRepo->delete($artist->id);

        $this->assertTrue($resp);
        $this->assertNull(artist::find($artist->id), 'artist should not exist in DB');
    }
}
