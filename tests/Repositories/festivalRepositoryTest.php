<?php namespace Tests\Repositories;

use App\Models\festival;
use App\Repositories\festivalRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class festivalRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var festivalRepository
     */
    protected $festivalRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->festivalRepo = \App::make(festivalRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_festival()
    {
        $festival = factory(festival::class)->make()->toArray();

        $createdfestival = $this->festivalRepo->create($festival);

        $createdfestival = $createdfestival->toArray();
        $this->assertArrayHasKey('id', $createdfestival);
        $this->assertNotNull($createdfestival['id'], 'Created festival must have id specified');
        $this->assertNotNull(festival::find($createdfestival['id']), 'festival with given id must be in DB');
        $this->assertModelData($festival, $createdfestival);
    }

    /**
     * @test read
     */
    public function test_read_festival()
    {
        $festival = factory(festival::class)->create();

        $dbfestival = $this->festivalRepo->find($festival->id);

        $dbfestival = $dbfestival->toArray();
        $this->assertModelData($festival->toArray(), $dbfestival);
    }

    /**
     * @test update
     */
    public function test_update_festival()
    {
        $festival = factory(festival::class)->create();
        $fakefestival = factory(festival::class)->make()->toArray();

        $updatedfestival = $this->festivalRepo->update($fakefestival, $festival->id);

        $this->assertModelData($fakefestival, $updatedfestival->toArray());
        $dbfestival = $this->festivalRepo->find($festival->id);
        $this->assertModelData($fakefestival, $dbfestival->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_festival()
    {
        $festival = factory(festival::class)->create();

        $resp = $this->festivalRepo->delete($festival->id);

        $this->assertTrue($resp);
        $this->assertNull(festival::find($festival->id), 'festival should not exist in DB');
    }
}
