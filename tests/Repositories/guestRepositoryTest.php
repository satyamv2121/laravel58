<?php namespace Tests\Repositories;

use App\Models\guest;
use App\Repositories\guestRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class guestRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var guestRepository
     */
    protected $guestRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->guestRepo = \App::make(guestRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_guest()
    {
        $guest = factory(guest::class)->make()->toArray();

        $createdguest = $this->guestRepo->create($guest);

        $createdguest = $createdguest->toArray();
        $this->assertArrayHasKey('id', $createdguest);
        $this->assertNotNull($createdguest['id'], 'Created guest must have id specified');
        $this->assertNotNull(guest::find($createdguest['id']), 'guest with given id must be in DB');
        $this->assertModelData($guest, $createdguest);
    }

    /**
     * @test read
     */
    public function test_read_guest()
    {
        $guest = factory(guest::class)->create();

        $dbguest = $this->guestRepo->find($guest->id);

        $dbguest = $dbguest->toArray();
        $this->assertModelData($guest->toArray(), $dbguest);
    }

    /**
     * @test update
     */
    public function test_update_guest()
    {
        $guest = factory(guest::class)->create();
        $fakeguest = factory(guest::class)->make()->toArray();

        $updatedguest = $this->guestRepo->update($fakeguest, $guest->id);

        $this->assertModelData($fakeguest, $updatedguest->toArray());
        $dbguest = $this->guestRepo->find($guest->id);
        $this->assertModelData($fakeguest, $dbguest->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_guest()
    {
        $guest = factory(guest::class)->create();

        $resp = $this->guestRepo->delete($guest->id);

        $this->assertTrue($resp);
        $this->assertNull(guest::find($guest->id), 'guest should not exist in DB');
    }
}
